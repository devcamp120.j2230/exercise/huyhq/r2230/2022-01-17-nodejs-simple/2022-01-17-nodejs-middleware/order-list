const getAllOrder = (req,res,next)=>{
    console.log("Get All Orders!");
    next();
};

const getAOrder = (req,res,next)=>{
    console.log("Get A Order!");
    next();
};

const postAOrder = (req,res,next)=>{
    console.log("Create New Order!");
    next();
};

const putAOrder = (req,res,next)=>{
    console.log("Update A Order!");
    next();
};

const deleteAOrder = (req,res,next)=>{
    console.log("Delete A Order!");
    next();
};

module.exports = {
    getAllOrder,
    getAOrder,
    postAOrder,
    putAOrder,
    deleteAOrder
}