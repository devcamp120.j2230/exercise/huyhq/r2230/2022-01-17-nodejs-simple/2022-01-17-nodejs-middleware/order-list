const express = require("express");
const {
    getAllOrder,
    getAOrder,
    postAOrder,
    putAOrder,
    deleteAOrder
} = require("../middlewares/orderMiddleware");

const orderRoute = express.Router();


orderRoute.get("/",getAllOrder,(req,res)=>{
    res.status(200).json({
        message: "Get All Orders!",
    })
})

orderRoute.get("/:id",getAOrder,(req,res)=>{
    var id = req.params.id;
    res.status(200).json({
        message: "Get Order id = "+id,
    })
})

orderRoute.post("/:id",postAOrder,(req,res)=>{
    var id = req.params.id;
    res.status(200).json({
        message: "Create Order id = "+id,
    })
})

orderRoute.put("/:id",putAOrder,(req,res)=>{
    var id = req.params.id;
    res.status(200).json({
        message: "Update Order id = "+id,
    })
})

orderRoute.delete("/:id",deleteAOrder,(req,res)=>{
    var id = req.params.id;
    res.status(200).json({
        message: "Delete Order id = "+id,
    })
})

module.exports = {orderRoute};